#include<iostream>            
using namespace std;
class Parent{
    public:
    int a=10;
    void dis()
    {
        cout<<"This Is A Parent Class"<<endl;
    }
};
class Child: public Parent{
    public:
    int b=20;
    void disp()
    {
        cout<<"This Is A Child Class"<<endl;
    }
};
int main()
{
    Child obj;
    cout<<obj.a<<endl;
    cout<<obj.b<<endl;
    obj.dis();
    obj.disp();
}
